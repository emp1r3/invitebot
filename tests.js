var chai = require('chai');
var expect = chai.expect; // we are using the "expect" style of Chai
var bot = require('./bot.js');
var config = require('./config.json');
var sinon = require('sinon');

var fetchUserStub;
var fetchGuildMemberStub;

before(function() {
  var rankId = 0;
  config.ranks.forEach(function(rank) {
    rank.id = rankId;
    rankId++;
  })
  bot.setRanks(config.ranks);
});

describe('Bot', function() {
  it('should display rank bronze', function() {
    var rank = bot.getTrueRank(3);
    expect(rank.name).to.equal('Bronze');
  });

  it('should display rank silver', function() {
    var rank = bot.getTrueRank(11);
    expect(rank.name).to.equal('Silver');
  });

  it('should return null', function() {
    var rank = bot.getTrueRank(0);
    expect(rank).to.equal(null);
  });

  it('should show rank up to bronze', function() {
    var message = generateMessageObject([]);
    bot.generateRankMessage(message, 2);
    expect(message.channel.send.calledWithMatch('You\'ve earned a rank up to Bronze! Congratulations!')).to.equal(true);
    expect(message.member.addRole.calledWithMatch(0)).to.equal(true);
  })

  it('should show rank up to gold from unranked', function() {
    var message = generateMessageObject([]);
    bot.generateRankMessage(message, 16);
    expect(message.channel.send.calledWithMatch('You\'ve earned a rank up to Gold! Congratulations!')).to.equal(true);
    expect(message.member.addRole.calledWithMatch(2)).to.equal(true);
  })

  it('should show rank up to gold', function() {
    var message = generateMessageObject([{name: 'Bronze'}]);
    bot.generateRankMessage(message, 16);
    expect(message.channel.send.calledWithMatch('You\'ve earned a rank up to Gold! Congratulations!')).to.equal(true);
    expect(message.member.addRole.calledWithMatch(2)).to.equal(true);
  })

  it('should show unranked', function() {
    var message = generateMessageObject([]);
    bot.generateRankMessage(message, 0);
    expect(message.channel.send.calledWithMatch('You\'re unranked. Invite some people to rank up!')).to.equal(true);
  })

  it('should say user is gold', function() {
    var message = generateMessageObject([{name: 'Gold'}]);
    bot.generateRankMessage(message, 0);
    console.log(message.channel.send.getCall(0).args[0]);
    expect(message.channel.send.calledWithMatch('You have invited 0 users. You\'re current rank is Gold.')).to.equal(true);
  })

  it('should say user is gold with bronze rank', function() {
    var message = generateMessageObject([{name: 'Gold'}]);
    bot.generateRankMessage(message, 1);
    console.log(message.channel.send.getCall(0).args[0]);
    expect(message.channel.send.calledWithMatch('You have invited 1 users. You\'re current rank is Gold.')).to.equal(true);
  })

  it('should say user is bronze appropriately', function() {
    var message = generateMessageObject([{name: 'Bronze'}]);
    bot.generateRankMessage(message, 1);

    expect(message.channel.send.calledWithMatch('You have invited 1 users. You\'re current rank is Bronze.')).to.equal(true);
  })

  it('should say user is gold with unknown rank', function() {
    bot.bot = {
      fetchUser: function(){}
    };
    var resolveUser = sinon.stub(bot.bot, 'fetchUser').resolves({
      "id": "123"
    });
    var message = generateMessageObject([{name: 'Gold'}]);
    bot.generateRankMessage(message, 0);

    expect(message.channel.send.calledWithMatch('You have invited 0 users. You\'re current rank is Gold.')).to.equal(true);
  })
});

var getUserRoles = function(roles, callback) {
  callback(roles);
}

var generateMessageObject = function(roles) {
  var message = {};
  message.channel = {
    send: function(){}
  };
  message.author = {
    id: '123'
  };
  sinon.stub(message.channel, 'send');
  message.member = {
    addRole: function(){}
  };
  sinon.stub(message.member, 'addRole');
  message.member.roles = {};
  message.member.roles.findKey = function(key, name) {
  var roleReturn = null;
  roles.forEach(function(role) {
      if(role.name && role.name === name) {
        roleReturn = role;
      }
    });
    return roleReturn;
  };
  return message;
}
