//This bot manages invites and permissions accordingly.

var Discord = require('discord.js');
var logger = require('winston');
var config = require('./config.json');
const axios = require('axios');
var token = 'Bot ' + config.token;
var cachedRanks = [];
// Configure logger settings
logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
    colorize: true
});
logger.level = 'info';
var date;

var bot;
var instance;

var init = function() {
 bot = new Discord.Client();
 bot.login(config.token);
  instance = axios.create({
    baseURL: config.discordUrl,
    timeout: 10000,
    headers: {'Authorization': token, 'User-Agent': 'DiscordBot (test, v1.0.0)'}
  });

  logger.info('Attempting to connect');

  bot.on('ready', function (evt) {
      logger.info('Connected');
      config.ranks.forEach(function(rank) {
        bot.guilds.forEach(function(guild) {
          guild.roles.forEach(function(role) {
              if(rank.name === role.name) {
                rank.id = role.id;
                cachedRanks.push(rank);
                logger.info('Pushed ' + JSON.stringify(rank));
              }
          });
        });
      });
  });

  bot.on('message', message => {
      if (message.content.substring(0, 1) == '!') {
          var args = message.content.substring(1).split(' ');
          var cmd = args[0];

          args = args.splice(1);
          switch(cmd) {
              case 'invite':
              handleInviteMessage(message);
              break;
              case 'pump':
              handleMassMessage(message);
              break;
           }
       }
  });
}


var getCurrentRank = function(userId, callback) {
 getUserRoles(userId, function(roles) {
    if(roles == null) {
      callback(null);
    }
    var lastRank;
      cachedRanks.some(function(rank) {
        if(roles.findKey('name', rank.name) != null) {
          lastRank = rank;
        }
      });
      callback(lastRank);
  });
}

var getTrueRank = function(numInvites) {
    if(numInvites == 0) {
      return null;
    }
    var lastRank;
    cachedRanks.some(function(rank) {
      logger.info('Checking for rank ' + rank.name + 'Num invites ' + numInvites + ' ' + rank.invitesNeeded);
        if(numInvites <= rank.invitesNeeded) {
            lastRank = rank;
            return true;
        }
    });
    return lastRank;
}

var messageChannel = function(channelName, message) {
    bot.channels.forEach(function (channel) {
        if(channel.name && channel.name === channelName) {
          channel.sendMessage(message);
        }
    });
}


var handleInviteMessage = function(message) {
  if(message.channel.name === config.inviteChannelName) {
  logger.info('Fetching invites');
  instance.get('/channels/' + message.channel.id + '/invites')
  .then(response => {
    logger.info('fetched invites for ' + message.channel.id);
    var totalInvites = 0;
    if(response.data) {
      response.data.forEach(function(channelInvite) {
        if(channelInvite.inviter.id === message.author.id) {
          totalInvites = totalInvites + channelInvite.uses;
        }
      });
    }
    generateRankMessage(message, totalInvites);
 })
  .catch(error => {
    console.log(error);
  });
}
}

var generateRankMessage = function(message, totalInvites) {
  var trueRank = getTrueRank(totalInvites);
  getCurrentRank(message.author.id, function(currentRank) {
    if(currentRank == null && trueRank == null) {
      message.channel.send('You\'re unranked. Invite some people to rank up!');
    } else if((trueRank != null && currentRank == null) || (trueRank != null && trueRank.rankNumber > currentRank.rankNumber)) {
      message.channel.send('You\'ve earned a rank up to ' + trueRank.name + '! Congratulations!');
      addRole(message.author.id, trueRank.id);
    } else {
      message.channel.send('You have invited ' + totalInvites + ' users. You\'re current rank is ' + currentRank.name + '.');
    }
  });
}

var addRole = function(userId, trueRankId) {
  bot.fetchUser(userId).then(user => {
    logger.info(user.toString());
    bot.guilds.forEach(function(guild) {
      var guildMemberPromise = guild.fetchMember(userId).then(guildMember => {
        guildMember.addRole(trueRankId);
      }).catch(error => {
        logger.info(error.toString());
      });
    });
  }).catch(error => {
    logger.info(error.toString())
  });
}

var getUserRoles = function(userId, callback) {
  bot.fetchUser(userId).then(user => {
    logger.info(user.toString());
    bot.guilds.forEach(function(guild) {
      var guildMemberPromise = guild.fetchMember(userId).then(guildMember => {
        callback(guildMember.roles);
      }).catch(error => {
        logger.info(error.toString());
      });
    });
  }).catch(error => {
    logger.info(error.toString())
  });
}

var handleMassMessage = function(message) {
    var canPump = false;
    var messageContent = message.content.split('pump')[1]
    getUserRoles(message.author.id, function(roles) {
      roles.forEach(function(role) {
        if(role.name === config.massMessageRole) {
            config.channels.forEach(function(channel) {
              logger.info('Pumping channel ' + channel.name + channel.delay +  messageContent);
              setTimeout(messageChannel, channel.delay, channel.name, 'Pump coin signal: ' + messageContent);
            });
        }
      });
    });
    // setTimeout(sendTelegramMessage, config.channels[0].delay, message);
}
//vvv
// var sendTelegramMessage = function(message) {
//   var methodUrl = config.telegram.url + config.telegram.name + ":" + config.telegram.token "/sendMessage";
//   var messageObj = {
//     "chat_id": config.telegram.channel;
//     "text": message
//   };
//   instance.post(methodUrl, messageObj);
// }

var setRanks = function(ranks) {
  cachedRanks = ranks;
}

var setTime = function(date) {
  if(date == 'clear')
    date = date;
}

var exports = module.exports = {};
exports.init = init;
exports.getCurrentRank = getCurrentRank;
exports.getTrueRank = getTrueRank;
exports.setRanks = setRanks;
exports.generateRankMessage = generateRankMessage;
exports.getUserRoles = getUserRoles;
exports.getCurrentRank = getCurrentRank;
exports.bot = bot;
